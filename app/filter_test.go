package app

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewFilters(t *testing.T) {
	t.Skip()
	tags := []tag{{
		Name:   "v1.0.0",
		Commit: "2695effb",
	}}

	filters := newFilters(tags, maxImageAge)

	require.Len(t, filters, 3)
	assert.Equal(t, reflect.ValueOf(filters[0]).Pointer(), reflect.ValueOf(isImageReleaseFilterFunc(tags)).Pointer())
	assert.Equal(t, reflect.ValueOf(filters[1]).Pointer(), reflect.ValueOf(isImageCommitReleaseFilterFunc(tags)).Pointer())
	assert.Equal(t, reflect.ValueOf(filters[2]).Pointer(), reflect.ValueOf(isImageOldFilterFunc(maxImageAge)).Pointer())
}

func TestIsImageReleaseFilterFunc(t *testing.T) {
	tags := []tag{{
		Name:   "v1.0.0",
		Commit: "2695effb",
	}}

	tests := []struct {
		tags              []string
		expectedImageType imageType
	}{
		{
			tags:              []string{"alpine3.12-x86_64-v1.0.0"},
			expectedImageType: imageTypeRelease,
		},
		{
			tags:              []string{"alpine3.12-x86_64-v1.0.1"},
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              nil,
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              []string{"-", "1.0.0"},
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              []string{"-----"},
			expectedImageType: imageTypeToDelete,
		},
	}

	fn := isImageReleaseFilterFunc(tags)
	for _, test := range tests {
		t.Run(fmt.Sprintf("%+v", test), func(t *testing.T) {
			assert.Equal(t, test.expectedImageType, fn(image{Tags: test.tags}))
		})
	}
}

func TestIsImageCommitReleaseFilterFunc(t *testing.T) {
	tags := []tag{{
		Name:   "v1.0.0",
		Commit: "2695effb",
	}}

	tests := []struct {
		tags              []string
		expectedImageType imageType
	}{
		{
			tags:              []string{"alpine3.12-x86_64-2695effb"},
			expectedImageType: imageTypeReleaseCommit,
		},
		{
			tags:              []string{"alpine3.12-x86_64-3695effb"},
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              nil,
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              []string{"-", "2695e"},
			expectedImageType: imageTypeToDelete,
		},
		{
			tags:              []string{"-----"},
			expectedImageType: imageTypeToDelete,
		},
	}

	fn := isImageCommitReleaseFilterFunc(tags)
	for _, test := range tests {
		t.Run(fmt.Sprintf("%+v", test), func(t *testing.T) {
			assert.Equal(t, test.expectedImageType, fn(image{Tags: test.tags}))
		})
	}
}

func TestIsImageOldFilterFunc(t *testing.T) {
	tests := []struct {
		pushedAt          string
		expectedImageType imageType
	}{
		{
			// pushed one month ago
			pushedAt:          time.Now().Add(-(time.Hour * 24 * 30)).Format(time.RFC3339),
			expectedImageType: imageTypeNotOld,
		},
		{
			// pushed now
			pushedAt:          time.Now().Format(time.RFC3339),
			expectedImageType: imageTypeNotOld,
		},
		{
			// pushed 24 hours before maxImageAge
			pushedAt:          time.Now().Add(-(time.Now().Sub(maxImageAge))).Add(-(time.Hour * 24)).Format(time.RFC3339),
			expectedImageType: imageTypeToDelete,
		},
	}

	fn := isImageOldFilterFunc(maxImageAge)
	for _, test := range tests {
		t.Run(fmt.Sprintf("%+v", test), func(t *testing.T) {
			pushedAt, _ := time.Parse(time.RFC3339, test.pushedAt)
			assert.Equal(t, test.expectedImageType, fn(image{PushedAt: TimeRFC3339{
				Time: pushedAt,
			}}))
		})
	}
}

func TestFiltersGroup(t *testing.T) {
	tags := []tag{
		{
			Name:   "v1.0.0",
			Commit: "2695effb",
		},
		{
			Name:   "v1.0.1",
			Commit: "3695effb",
		},
	}

	images := []image{
		{
			// not old enough
			PushedAt: TimeRFC3339{
				Time: time.Now(),
			},
			Tags: []string{"alpine3.12-x86_64-5695effb"},
		},
		{
			// pushed a year ago but is a release tag
			PushedAt: TimeRFC3339{
				Time: time.Now().Add(-(time.Hour * 24 * 30 * 12)),
			},
			Tags: []string{"alpine3.12-x86_64-v1.0.0"},
		},
		{
			// pushed a year ago but is a release commit
			PushedAt: TimeRFC3339{
				Time: time.Now().Add(-(time.Hour * 24 * 30 * 12)),
			},
			Tags: []string{"alpine3.12-x86_64-3695effb"},
		},
		{
			// pushed a year ago and is not release related
			PushedAt: TimeRFC3339{
				Time: time.Now().Add(-(time.Hour * 24 * 30 * 12)),
			},
			Tags: []string{"alpine3.12-x86_64-something"},
		},
	}

	groups := newFilters(tags, maxImageAge).group(images)

	require.Len(t, groups[imageTypeNotOld], 1)
	assert.Equal(t, groups[imageTypeNotOld][0].Tags, []string{"alpine3.12-x86_64-5695effb"})

	require.Len(t, groups[imageTypeRelease], 1)
	require.Equal(t, groups[imageTypeRelease][0].Tags, []string{"alpine3.12-x86_64-v1.0.0"})

	require.Len(t, groups[imageTypeReleaseCommit], 1)
	require.Equal(t, groups[imageTypeReleaseCommit][0].Tags, []string{"alpine3.12-x86_64-3695effb"})

	require.Len(t, groups[imageTypeToDelete], 1)
	require.Equal(t, groups[imageTypeToDelete][0].Tags, []string{"alpine3.12-x86_64-something"})

}
