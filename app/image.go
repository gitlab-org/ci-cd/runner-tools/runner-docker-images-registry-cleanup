package app

type imageType int

const (
	imageTypeRelease imageType = iota
	imageTypeReleaseCommit
	imageTypeNotOld
	imageTypeToDelete
)

var imageTypeToCacheName = map[imageType]func(string) cacheName{
	imageTypeRelease:       cacheNameReleaseImages,
	imageTypeReleaseCommit: cacheNameReleaseCommitImages,
	imageTypeNotOld:        cacheNameNotOldImages,
	imageTypeToDelete:      cacheNameToDeleteImages,
}

type image struct {
	PushedAt TimeRFC3339 `json:"pushed_at"`
	Tags     []string    `json:"tags"`
	Digest   string      `json:"digest"`
}
