package app

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

func TestNewAPI(t *testing.T) {
	api := NewAPI()

	assert.Equal(t, api.client.Header.Get("PRIVATE-TOKEN"), "")
	assert.Equal(t, api.gitlabAddr.String(), "https://gitlab.com")

	api = NewAPI().WithToken("token")
	assert.Equal(t, api.client.Header.Get("PRIVATE-TOKEN"), "token")
}

func TestAPIUrls(t *testing.T) {
	api := NewAPI()

	assert.Equal(t, api.projectURL().String(), "https://gitlab.com/api/v4/projects/250833")
	assert.Equal(t, api.tagsURL().String(), "https://gitlab.com/api/v4/projects/250833/repository/tags")
}

func mockAPI(t *testing.T) (*API, func()) {
	api := NewAPI()
	b, err := os.ReadFile("test/tags_test.json")
	require.NoError(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("x-total-pages", "1")
		_, _ = writer.Write(b)
	}))

	api.gitlabAddr, _ = url.Parse(server.URL)

	return api, server.Close
}

func TestAPIFetchTags(t *testing.T) {
	api, cleanup := mockAPI(t)
	defer cleanup()

	tags, err := api.fetchTags()
	require.NoError(t, err)

	assert.Len(t, tags, 1)
	assert.Equal(t, tags[0].Commit, "2695effb")
	assert.Equal(t, tags[0].Name, "v1.0.0")
}
