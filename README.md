# GitLab Runner Docker images registry cleanup

This project is intended to solve https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29424 and https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29070.

It's currently impossible to find all images we don't need simply through a regex since we tag a lot of images that simply include a short commit sha this is a possible
workaround until we move entirely to pushing only version tagged helper images and use short sha for dev builds.

To get more context on the types and names of images we push refer to this comment: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29070#note_1055812082.

----

This project aims specifically to remove all images that:

* Do not contain an existing GitLab Runner tag: The tags are collected from https://gitlab.com/gitlab-org/gitlab-runner/-/tags and all ECR images are compared against them. If a tag doesn't exist in Runner it can be removed. That could happen if a tag is created by mistake and then deleted but the images stayed. Refer to `image.isRelease`.
* Do not contain a short git sha pointing to a Runner tag: For example `x86_64-775dd39d-servercore1809` is such an image. It points to release `v13.8.0` and it's tagged with the `v13.8.0` tag, but it's most likely being referenced by some GitLab Runner somewhere out there so we can't remove it.
* Is older than 6 months: If we prove that an image is not a release image then it is a dev build from MR or from a branch pipeline. If the image is older than 6 months we should be safe to remove it. 

All images are written in separate files for detailed inspection in the `cache` directory:

* `ecr-images-gitlab-runner-helper.jsonl`: All images  we currently have in the `gitlab-runner-helper` ECR repository.
* `release-images-gitlab-runner-helper.jsonl`: All images that contain a release tag and will not be removed for `gitlab-runner-helper`.
* `release-commit-images-gitlab-runner-helper.jsonl`: All images that contain a release commit and will not be removed for `gitlab-runner-helper`.
* `not-old-images-gitlab-runner-helper.jsonl`: All images that are not considered old and will not be removed for `gitlab-runner-helper`.
* `to-delete-images-gitlab-runner-helper.jsonl`: All images that did not pass the above criteria and should be removed for `gitlab-runner-helper`.

The goal is to get visibility into the images we want to remove and to verify that the requirements above and the assumptions about them are correct and verify that the execution is correct too.

# Setup

Since the GitLab Runner repo is public no GitLab API access token is needed to fetch the tags.

`aws ecr-public` requires authentication with the following permissions. 

```
ecr-public:DescribeImages on resource: arn:aws:ecr-public::782774275127:repository/gitlab-runner-helper
```
```
ecr-public:DescribeImages on resource: arn:aws:ecr-public::782774275127:repository/gitlab-runner
```

Make sure to set it to the correct region: `aws configure set default.region us-east-1`.

Run the tool with `go run . -find -registry ecr -repository gitlab-runner-helper`. You should get output similar to:

```
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Loading cache tags.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Loading cache ecr-gitlab-runner-helper.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Tags: 744"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Images: 5674"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Writing cache release-commit-images-gitlab-runner-helper.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"release-commit-images-gitlab-runner-helper.jsonl: 254"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Writing cache release-images-gitlab-runner-helper.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"release-images-gitlab-runner-helper.jsonl: 3100"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Writing cache not-old-images-gitlab-runner-helper.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"not-old-images-gitlab-runner-helper.jsonl: 1976"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"Writing cache to-delete-images-gitlab-runner-helper.jsonl"}
{"level":"info","time":"2022-12-01T02:25:43+02:00","message":"to-delete-images-gitlab-runner-helper.jsonl: 344"}
 ```

# CLI parameters

```
  -access-token string
    	gitlab access token
  -delete
    	delete images passed through stdin line by line
  -dry-run
    	do a dry run of images removal
  -find
    	find images to cleanup
  -registry string
    	registry to search in or delete from. Values: ecr, gitlab
  -repository string
    	repository name to search in or delete from. Values: gitlab-runner, gitlab-runner-helper
```

# ECR keys infrastructure setup

This the repository [ecr-public](https://gitlab.com/gitlab-org/ci-cd/distribution/runner/infrastructure/aws/ecr-public) to create the initial credentials setup.

# Obtaining AWS keys

As of the time of writing this the ECR keys to list and delete images in the gitlab-runner and gitlab-runner-helper repos are stored in 1Password in the verify group section named **"AWS ECR Runner List/Delete Key"**

# Refreshing AWS keys

In case the keys get invalidated follow the following steps to obtain new ones:

1. Open an access request at https://gitlab.com/gitlab-com/team-member-epics/access-requests
1. Follow the example of the previous one https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/33937
1. Test the keys locally by setting them in your `~/.aws/credentials` file and running the cleanup tool locally
1. Set the keys in the environment of this project
1. Run a scheduled pipeline to verify it's working correctly

