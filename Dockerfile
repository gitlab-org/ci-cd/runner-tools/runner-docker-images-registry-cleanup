FROM golang:1.23.4-bookworm

WORKDIR /app
ADD . .

RUN go test ./... -v

RUN go build -o /usr/local/bin/ic

RUN apt-get update -y && \
    apt-get install wget zip -y

RUN wget -q https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.4.19.zip -O awscliv2.zip && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -r aws && \
    rm awscliv2.zip

RUN ic -help && aws --version
